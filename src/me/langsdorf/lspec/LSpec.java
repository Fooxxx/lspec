package me.langsdorf.lspec;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class LSpec extends JavaPlugin {
	
	@Override
	public void onEnable() {
		log("�aLSpec ativado.");
		log("�a1.0");
		
	}
	
	public void log(String log) {
		Bukkit.getConsoleSender().sendMessage(log);
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player)sender;
			if (label.equalsIgnoreCase("spec")) {
				if (p.hasPermission("lspec.comando")) {
					if (args.length == 0) {
						p.sendMessage("�6/spec <player>");
						p.sendMessage("�6/spec sair");
						return true;
					} else {
						if (args.length == 1) {
							Player p2 = Bukkit.getPlayer(args[0]);
							if (p2 != null) {
								API.setEspec(p, p2);
								p.sendMessage("�aVoc� est� espectando: " + p2.getName());
							} else {
								p.sendMessage("�cPlayer n�o encontrado!");
								return true;
							}
						}
						if (args[0].equalsIgnoreCase("sair")) {
							API.removeEspec(p, GameMode.SURVIVAL);
							p.sendMessage("�cVoc� saiu do modo de espectador!");
						}
					}
				} else {
					p.sendMessage("�cSem permiss�o!");
					return true;
				}
			}
		}
		return false;
	}

}
