package me.langsdorf.lspec;

import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R1.Packet;
import net.minecraft.server.v1_8_R1.PacketPlayOutCamera;

public class API {
	
	
	
	public static void setEspec(Player p, Player p2) {
          PacketPlayOutCamera c = new PacketPlayOutCamera();
          c.a = p2.getEntityId();
          p.setGameMode(GameMode.SPECTATOR);
          ((CraftPlayer)p).getHandle().playerConnection.sendPacket((Packet)c);
	}
	
	public static void removeEspec(Player p, GameMode g) {
         PacketPlayOutCamera c = new PacketPlayOutCamera();
         c.a = p.getEntityId();
         ((CraftPlayer)p).getHandle().playerConnection.sendPacket((Packet)c);
         p.setGameMode(g);
	}
	
}
